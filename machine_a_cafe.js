import Discord from 'discord.js';

// PARAMÈTRES À RENSEIGNER IMPÉRATIVEMENT.
const token = 'COLLEZ_LE_JETON_ICI'; // Jeton d'identification.

// Paramètres optionnels.
const channelId = ''; // Identifiant du salon de la machine à café (laisser vide pour transporter la machine à café dans toutes les pièces).
const commande = '!40centimes'; // Commande (prix) pour lancer le bot.
const boissonsCourtes = ['cafecapsule', 'cafemoulu', 'chocolat', 'chocolait', 'chocomenthe', 'choconoisette', 'thevert']; // Boissons préparées rapidement.
const boissonsLongues = ['cafegrains']; // Boissons préparées lentement.
const tempsCourt = 20000; // Temps de conception (en millisecondes) des boissons préparées rapidement.
const tempsLong = 40000; // Temps de conception (en millisecondes) des boissons préparées lentement.
const heureOuverture = 8; // Heure d'ouverture de la machine à café.
const heureFermeture = 17; // Heure de fermeture de la machine à café.
const joursFermeture = [0, 6]; // Jours de fermeture de la machine à café (Dimanche = 0, Lundi = 1, ... Samedi = 6).
const probaAccident = 2; // Pourcentages d'accidents nécessitant de !nettoyer le sol avant de se resservir.

const client = new Discord.Client();
let encours = false;
let nettoyer = false;

client.login(token);

function cafe(msg, time) {
  encours = true;
  msg.channel.send('*gobelet qui tombe*').catch(console.error);
  setTimeout(() => {
    msg.channel.send('**BVUUUUU**').catch(console.error);
    setTimeout(() => {
      if (msg.content.indexOf('sucre') !== -1) {
        msg.channel.send('*sucre ajouté*').catch(console.error);
      }
      msg.channel.send('**BIIIIP**').catch(console.error);
      encours = false;
      if ((Math.floor(Math.random() * 100) + 1) <= probaAccident) {
        setTimeout(() => {
          nettoyer = true;
          msg.channel.send('**PLOUF** oh non, le café est tombé par terre, il faut !nettoyer.').catch(console.error);
        }, 3000);
      }
    }, time);
  }, 2000);
}

client.on('message', (msg) => {
  if ((channelId === '') || (msg.channel.id === channelId)) {
    if (msg.content.indexOf(commande) !== -1) {
      const d = new Date();
      if ((d.getHours() >= heureOuverture) && (d.getHours() < heureFermeture) && (joursFermeture.indexOf(d.getDay()) === -1)) {
        if (nettoyer) {
          msg.channel.send('Il faut !nettoyer le café par terre avant de se resservir.').catch(console.error);
        } else if (encours) {
          msg.channel.send('On coupe pas la file d\'attente SVP.').catch(console.error);
        } else {
          boissonsCourtes.some((boissonCourte) => {
            if (msg.content.indexOf(boissonCourte) !== -1) {
              cafe(msg, tempsCourt);
              return true;
            }
            return false;
          });
          boissonsLongues.some((boissonlongue) => {
            if (msg.content.indexOf(boissonlongue) !== -1) {
              cafe(msg, tempsLong);
              return true;
            }
            return false;
          });
          if (!encours) {
            msg.channel.send(`Liste des boissons : ${boissonsCourtes.join(', ')}, ${boissonsLongues.join(', ')}.`).catch(console.error);
          }
        }
      } else if ((d.getHours() < heureOuverture) || (d.getHours() >= heureFermeture) || (joursFermeture.indexOf(d.getDay()) !== -1)) {
        msg.channel.send('Notre établissement est fermé, prenez votre café chez vous.').catch(console.error);
      } else {
        msg.channel.send('La machine à café n\'est pas dans cette salle, vos pièces sont tombées par terre.').catch(console.error);
      }
    } else if ((nettoyer) && (msg.content.indexOf('!nettoyer') === 0)) {
      nettoyer = false;
      msg.channel.send('Merci d\'avoir nettoyé !').catch(console.error);
    }
  }
});

process.on('error', (err) => { console.log(err); });
